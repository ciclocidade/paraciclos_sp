
## Contexto

Em 2016, o [Plano de Mobilidade de São Paulo](https://www.prefeitura.sp.gov.br/cidade/secretarias/upload/chamadas/planmobsp_v072__1455546429.pdf) estabeleceu a meta de "*instalar 4.000 paraciclos ao longo de toda a Rede Cicloviária Estrutural em sua proporção implantada*" até aquele mesmo ano. É importante observar que a meta se refere à implantação **ao longo da rede cicloviária**, não cobrindo outras porções da cidade. Em 2020, reconhecendo que o objetivo havia sido cumprido até a metade, o [Plano Cicloviário do Município de São Paulo](http://www.cetsp.com.br/media/1100812/Plano-Ciclovia%CC%81rio_2020.pdf) estendeu o prazo para "*instalar novos 2.000 paraciclos até 2024*".

O problema até o ano de 2022 é que não havia um levantamento atualizado da localização de tais estruturas. A Ciclocidade, em sua [Auditoria Cidadã 2022 da Estrutura Cicloviária de São Paulo](https://www.ciclocidade.org.br/wp-content/uploads/2022/07/Auditoria-Cidada-Estrutura-Cicloviaria-SP-2022.pdf), percorreu toda a malha existente para mapeá-las, publicando o arquivo *shapefile* no [GitLab](https://gitlab.com/ciclocidade/auditoria_cidada_2022) da associação. 

**Mapa dos paraciclos ao longo da rede cicloviária existente. Fonte: [Ciclocidade, 2022](https://www.ciclocidade.org.br/wp-content/uploads/2022/07/Auditoria-Cidada-Estrutura-Cicloviaria-SP-2022.pdf).**
<div align="center">
<img src="/images/auditoria_cidada_2022_paraciclos.jpg" alt="Auditoria Cidadã 2022 - Paraciclos" width="600"/>
</div>

<br>O mapeamento evidenciou que embora haja eixos muito bem abastecidos dos diferentes tipos de paraciclos (u invertido padrão, u invertido com barra, *sheffield* etc.), também há completos vazios em que quilômetros inteiros ficam sem vagas de estacionamento de curta duração.


<br>

## Sobre este repositório

O objetivo deste repositório é o de **embasar uma proposta** para que os 2 mil novos paraciclos a serem implantados até 2024 estejam distribuídos ao longo da rede de forma a fornecer estruturas em áreas onde não existem em número proporcional à demanda esperada para a localidade. Para tanto, são seguidos os seguintes passos:

**Script 1**. O território da cidade de São Paulo é dividido em **áreas hexagonais [H3](https://h3geo.org/docs/highlights/indexing) de resolução 8**, o equivalente a uma [área média de 0,737 km²](https://h3geo.org/docs/core-library/restable/) e a uma extensão em torno de 1,1 km entre as pontas opostas de cada hexágono. O objetivo deste passo é obter resoluções melhores, mais específicas, do que as alcançadas pelas zonas OD, em especial para zonas não centrais da cidade.

**Comparativo de áreas: Hexágonos H3 em resolução 8 e Zonas OD.**
<div align="center">
<img src="/images/comparativo_hexagonos_zonas_od.jpg" alt="Comparativo Hexágonos Zonas OD" width="600"/>
</div>


<br>**Scripts 2 e 3**. As **viagens atraídas**, ou seja, todas as viagens que não possuem como motivo no destino a residência da pessoa, são agrupadas primeiro por zona origem e destino e, depois, distribuídas proporcionalmente de acordo com as áreas dos hexágonos que interseccionam com tais zonas. A fonte dos dados de viagem é a [Pesquisa Origem e Destino 2017, do Metrô de SP](https://www.metro.sp.gov.br/pesquisa-od/).

**Viagens atraídas agrupadas por hexágono H3.**
<div align="center">
<img src="/images/viagens_atraidas_por_hexagono.jpg" alt="Viagens atraídas por hexágono" width="600"/>
</div>

<br>**Script 4**. As **quantidades de paraciclos e de vagas são associadas a cada hexágono**. Hexágonos com infraestrutura cicloviária de circulação (ciclovias, ciclofaixas e ciclorrotas) são marcados. A fonte dos dados de paraciclos e vagas, bem como a rede cicloviária existente, são os arquivos *shapefile* da Auditoria Cidadã 2022 publicados no [GitLab da Ciclocidade](https://gitlab.com/ciclocidade/auditoria_cidada_2022).

<br>**Script 5**. Finalmente, a **quantidade de vagas existentes em cada hexágono é subtraída da demanda calculada por vagas**, sendo esta uma proporção da quantidade total de viagens atraídas.

Os números negativos (onde há déficit de vagas) são então arredondados e tornados pares, para que possam corresponder a unidades de paraciclo em u invertido, formato que possui duas vagas. Em outras palavras, se em um hexágono há demanda estimada por 4.8 vagas e nele há somente 1 paraciclo com 2 vagas, o resultado será arredondado (-2.8 para -3.0) e tornado par (de -3 para -4) para que o número seja calculado em estruturas padrão de paraciclo (2 vagas por u invertido).

Os scripts estão todos em linguagem [R](https://www.r-project.org/). Os mapas para visualização dos dados são feitos no software [QGIS](https://www.qgis.org/en/site/).


<br>

## Bases de dados necessárias para rodar os scripts

1. Mapa da cidade de São Paulo e base de dados de resultados da Pesquisa OD 2017. Arquivos em formato .shp e .dbf, respectivamente - o primeiro precisa ser ajustado no [QGIS](https://www.qgis.org/en/site/) (informações no script 01). Onde baixar? [Site do Metrô de SP](https://www.metro.sp.gov.br/pesquisa-od/);

2. Localização dos paraciclos e rede cicloviária existente ao primeiro semestre de 2022. Arquivos em formato .gpkg. Onde baixar? [GitLab da Ciclocidade](https://gitlab.com/ciclocidade/auditoria_cidada_2022). Redes mais atuais devem ser baixadas da plataforma de dados abertos [GeoSampa](http://geosampa.prefeitura.sp.gov.br/PaginasPublicas/_SBC.aspx).


<br>

## Alguns dados da Pesquisa OD 2017

| Considerado o total de viagens atraídas para SP (25.850.703) | Proporcional | Total | % do total | 
|:--|--:|--:|--:|
|Viagens em bicicleta | 1 a cada 117 viagens | 221.785 | 0,86% |
|Viagens em bicicleta que param na rua (com/sem guardador) ou em paraciclos | 1 a cada 2.064 viagens | 12.525 | 0,05% |
|Viagens em bicicleta que param em bicicletários pagos/gratuitos | 1 a cada 1.127 viagens | 24.523 | 0,09% |
|Viagens em bicicleta que param em paraciclos/bicicletários | 1 a cada 698 viagens | 37.048 | 0,14% |
|Viagens em bicicleta que param em locais privados/outros | 1 a cada 140 viagens | 184.738 | 0,71% |


<br>

## Resultados

De acordo com a OD2017, há demanda de 1 vaga de curta duração (na rua, em paraciclos) para cada 2.064 viagens totais. Para o cálculo de demanda, o número foi arredondado para 1 a cada 2.000 viagens. Os resultados podem ser vistos na imagem abaixo. Para a visualização final, são selecionados apenas os hexágonos em que há infraestrutura cicloviária de circulação existente.

**Demanda de paraciclos proporcional à demanda em hexágonos com infraestrutura cicloviária (1 paraciclo = 2 vagas).**
<div align="center">
<img src="/images/demanda_paraciclos_por_hexagonos.jpg" alt="Demanda de paraciclos por hexágono" />
</div>

<br>Considerados somente os hexágonos em que há infraestrutura cicloviária de circulação, **para cobrir a demanda são necessárias 2.980 vagas, ou 1.490 paraciclos em formato u invertido**, um número que está dentro da meta de implantação estipulada para o ano de 2024.

O cálculo ainda não considera a rede cicloviária que está em fase atual de implantação e a que está prevista para ser construída até 2024 - de acordo com o [Programa de Metas 21/24](https://www.prefeitura.sp.gov.br/cidade/secretarias/upload/governo/arquivos/programa_de_metas/programa-de-metas-2021-2024/pdm.relatorio.versao.final.participativa.pdf), serão 300 km novos nos próximos 2 anos.

